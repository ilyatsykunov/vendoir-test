const app = require('../index');
const expect = require('chai').expect;
const request = require('supertest');

describe('GET /timeastext/', () => {
    it('Should return status code 200 for current time', () => {
        request(app).get('/timeastext/').expect(200);
    });
});

describe('POST /timeastext/15:30', () => {
    it('Shoud return status code 200 for input time', async () => {
        const res = await request(app).post('/timeastext/15:30').send({time: '15:30'});
        expect(res.statusCode).equal(200);
    });
});

describe('POST /timeastext/00:59', () => {
    it('Shoud return converted input time 1', async () => {
        const res = await request(app).post('/timeastext/15:30').send({time: '15:30'});
        expect(res.body).to.have.property('time');
        expect(res.body.time).equal('Half past three');
    });
});

describe('POST /timeastext/15:30', () => {
    it('Shoud return converted input time 2', async () => {
        const res = await request(app).post('/timeastext/00:59').send({time: '00:59'});
        expect(res.body).to.have.property('time');
        expect(res.body.time).equal('One to one');
    });
});

describe('POST /timeastext/08:00', () => {
    it('Shoud return converted input time 3', async () => {
        const res = await request(app).post('/timeastext/08:00').send({time: '08:00'});
        expect(res.body).to.have.property('time');
        expect(res.body.time).equal('Eight o\'clock');
    });
});