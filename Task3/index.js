const { timeAsText } = require("./timeAsText");
const app = require('express')();
module.exports = app;

// Current time
app.get('/timeastext', (req, res) => {
    const time = (() => {
        const dt = new Date();
        return `${dt.getHours()}:${dt.getMinutes()}`;
    })();
    res.status(200).send({
        time: timeAsText(`${time}`)
    })
});

// Converted time
app.post('/timeastext/:time', (req, res) => {
    const { time } = req.params;

    res.send({
        time: timeAsText(`${time}`)
    });
});