/** Converts current time to human friendly format
 * @returns {string} time in human friendly format 
 */
function curTimeAsText() {
    const hours = { 0: 'twelve', 1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'one', 14: 'two', 15: 'three', 16: 'four', 17: 'five', 18: 'six', 19: 'seven', 20: 'eight', 21: 'nine', 22: 'ten', 23: 'eleven', 24: 'twelve' };
    const minutes = { 0: 'o\'clock', 1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen', 14: 'fourteen', 15: 'quarter', 16: 'sixteen', 17: 'seventeen', 18: 'eighteen', 19: 'nineteen', 20: 'twenty', 21: 'twenty one', 22: 'twenty two', 23: 'twenty three', 24: 'twenty four', 25: 'twenty five', 26: 'twenty six', 27: 'twenty seven', 28: 'twenty eight', 29: 'twenty nine', 30: 'half' };

    const output = (() => {
        const dt = new Date();
        let hr = parseInt(dt.getHours());
        let min = parseInt(dt.getMinutes());
        if (min == 0) {
            return `${hours[hr]} ${minutes[min]}`;
        }
        else if (min <= 30) {
            return `${minutes[min]} past ${hours[hr]}`;
        } else if (min > 30) {
            hr++;
            min = 60 - min;
            return `${minutes[min]} to ${hours[hr]}`;
        }
    })();
    return `${output[0].toUpperCase()}${output.slice(1)}`;
}

exports.curTimeAsText = curTimeAsText;

console.log(curTimeAsText());
