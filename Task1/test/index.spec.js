const { curTimeAsText } = require('../index');
const expect = require('chai').expect;

describe('Current Time as Text', () => {
    it('Shoud not return null', () => {
        const output = curTimeAsText();
        expect(output).not.equal(undefined);
        expect(output).not.equal(null);
    });
});
