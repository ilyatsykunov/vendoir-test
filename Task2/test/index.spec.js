const { timeAsText } = require('../index');
const expect = require('chai').expect;

describe('Time as Text', () => {
    it('Shoud return error on empty input', () => {
        const output = timeAsText('');
        expect(output).equal('Error: check input.');
    });
});

describe('Time as Text', () => {
    it('Shoud return correct time 1', () => {
        const output = timeAsText('15:30');
        expect(output).equal('Half past three');
    });
});

describe('Time as Text', () => {
    it('Shoud return correct time 2', () => {
        const output = timeAsText('21:59');
        expect(output).equal('One to ten');
    });
});

describe('Time as Text', () => {
    it('Shoud return correct time 3', () => {
        const output = timeAsText('00:00');
        expect(output).equal('Twelve o\'clock');
    });
});

describe('Time as Text', () => {
    it('Shoud return correct time 4', () => {
        const output = timeAsText('00:00');
        expect(output).not.equal('00:00');
    });
});